<?
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

\Bitrix\Main\Diag\Debug::dumpToFile(array($_REQUEST, $_SERVER));

\CBitrixComponent::includeComponentClass("newsite:webserviceparams");
\CBitrixComponent::includeComponentClass("newsite:webservicefabrica");

$obWebServiceParams = new WebServiceParams($_GET, $_POST, $_SERVER);

if( !$obWebServiceParams->isAuth() ) {
    LocalRedirect("/");
    die;
}

$obWebServiceAction = WebServiceFabrica::getActionClass($obWebServiceParams->getAction());

foreach( $obWebServiceAction->getKeyDataRequest() as $key ) {
    $obWebServiceAction->setValueDataRequest( $key, $obWebServiceParams->getValueDataForKeyRequest($key) );
}
print json_encode($obWebServiceAction->getData());
die;
