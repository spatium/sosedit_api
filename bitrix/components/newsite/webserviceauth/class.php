<?php

class WebServiceAuth
{
    const P_LOGIN   = 'email';
    const P_PASSW   = 'password';
    
    const E_NOVALID = 1; // неверный логин и пароль
    
    protected $data;
    protected $result;
    protected $user;
    
    public function __construct() {
        $this->data = array(
         'email',
         'password',
        );
        
        $this->result = array();
        $this->user = array();
    }
    
    public function getKeyDataRequest() {
        return $this->data;
    }
    
    public function setValueDataRequest($_key, $_value) {
        $this->data[$_key] = $_value;
    }
    
    public function returnError($_type) {
        return $this->result = array(
         "error" => $_type,
         "status" => 0
        );
    }
    
    public function getData() {
        if( strlen($this->data[self::P_LOGIN]) <= 0 || strlen($this->data[self::P_PASSW]) <= 0 ) {
            return $this->returnError(self::E_NOVALID);
        }
        
        $this->user = \CUser::GetByLogin($this->data[self::P_LOGIN])->fetch();
        if( !$this->validData() ) {
            return $this->returnError(self::E_NOVALID);
        }
        
        return $this->result = array(
            "data" => array("id" => 'ЛК#'.$this->user['ID'].'#'),
            "status" => 0,
        );
    }
    
    protected function validData() {
        $salt = substr($this->user['PASSWORD'], 0, (strlen($this->user['PASSWORD']) - 32));

        $realPassword = substr($this->user['PASSWORD'], -32);
        $password = md5($salt.$this->data[self::P_PASSW]);

        return ($password == $realPassword);
    }
}