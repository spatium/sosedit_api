<?php

class WebServiceFabrica
{
    const W_AUTH = 'auth';
    
    protected function __construct() {
    
    }
    
    public function getActionClass($_action) {
        switch($_action)
        {
            case self::W_AUTH:
                \CBitrixComponent::includeComponentClass("newsite:webserviceauth");
                return new WebServiceAuth;
        }
        
    }    
}