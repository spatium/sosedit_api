<?php

class WebServiceParams
{
    protected $GET;
    protected $POST;
    protected $SERVER;
    
    private $ip;
    
    public function __construct($GET, $POST, $SERVER) {
        $this->GET  = $GET;
        $this->POST = $POST;
        $this->SERVER = $SERVER;
        
        $this->ip = '191.238.117.87';
    }
    
   /*
    * Функция проверяет с того ли домена пришел нам запрос
    */
    public function isAuth() {
        return isset($this->SERVER["REMOTE_ADDR"]) && $this->ip == $this->SERVER["REMOTE_ADDR"];
    }
    
    public function getValueDataForKeyRequest($_key) {
        return htmlspecialchars($this->POST[$_key]);
    }
    
    public function getAction() {
        return htmlspecialchars($this->GET['ACTION']);
        
    }
}